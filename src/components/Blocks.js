import React from 'react';

const Blocks = props => {
    return (
        <div>
            {props.array.map((block, k) => <div onClick={props.gameState} id={k} className={block.isShown ? 'block opened' : 'block'} key={k}>{block.hasItem ? 'O' : null}</div>)}
        </div>
    )
};

export default Blocks;