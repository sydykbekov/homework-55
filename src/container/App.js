import React, {Component} from 'react';
import './App.css';
import Chances from '../components/Chances';
import Reset from '../components/Reset';
import Title from '../components/Title';
import Blocks from '../components/Blocks';

class App extends Component {
    state = {
        array: [],
        chances: 0,
        gameState: false
    };

    componentDidMount() {
        this.resetGame();
    };

    handleClick = (event) => {
        let gameState;
        let elem = event.target;
        let id = elem.getAttribute('id');
        let array = [...this.state.array];
        let chances = this.state.chances;
        if (!array[id].isShown) {
            array[id].isShown = true;
            chances++;
        }
        if (array[id].hasItem) {
            gameState = true;
        }
        this.setState({array, chances, gameState});
    };

    resetGame = () => {
        let gameState = false;
        let array = [];
        while (array.length < 36) {
            let obj = {
                hasItem: false,
                isShown: false
            };
            array.push(obj);
        }
        let chances = 0;
        let randomNum = Math.floor(Math.random() * array.length);
        array[randomNum].hasItem = true;
        this.setState({array, chances, gameState});
    };

    render() {
        return (
            <div className="App">
                <Title title={this.state.gameState ? 'Congratulations! You found the ring!' : 'Try to find the ring!'}/>
                <Blocks array={this.state.array} gameState={this.state.gameState ? null : this.handleClick} />
                <Chances chances={this.state.chances}/>
                <Reset resetGame={this.resetGame}/>
            </div>
        );
    }
}

export default App;
