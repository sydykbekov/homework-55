import React from 'react';

const Chances = (props) => {
    return (
        <p>Tries: {props.chances}</p>
    )
};

export default Chances;