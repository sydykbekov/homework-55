import React from 'react';

const Reset = props => {
    return (
        <button onClick={props.resetGame}>Reset</button>
    )
};

export default Reset;